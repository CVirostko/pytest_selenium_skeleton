import pytest

from src.page_objects.google.google_page import GooglePage


@pytest.mark.usefixtures('before_and_after_all_ui', 'before_after_each')
@pytest.mark.selenium
class TestProject1:

    @pytest.mark.P1
    def test_google_search(self):
        search_text = 'workfront'
        google_page = GooglePage(self)
        google_page.navigate()
        google_page.set_search_text(search_text)
        google_page.search_btn_click()
        assert google_page.search_results_exist() is True
