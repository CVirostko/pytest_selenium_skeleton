pytest
pytest-html
pytest-rerunfailures
pytz
requests
selenium
urllib3
webdriver-manager==2.2.0