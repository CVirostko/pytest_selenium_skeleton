import logging
import pytest
import time

from selenium import webdriver
from src.helpers.webdriver_helper import setup_webdriver, take_screenshot


@pytest.fixture
def before_after_each(request):
    ui_marker = 'selenium'

    logging.info("{0} Begins".format(request.node.name))

    if request.node.get_closest_marker(ui_marker):
        request.cls.driver = request.session.driver

    yield

    if request.node.rep_call.failed and request.node.get_closest_marker(ui_marker):
        take_screenshot(request.session.driver, request.node.name)

    logging.info("{0} Finished".format(request.node.name))


@pytest.fixture(scope='session')
def before_and_after_all_ui(request):
    logging.info("Global UI Test Setup Begins")

    driver = setup_ui_test(request)

    logging.info('Global UI Test Setup has finished')

    yield

    logging.info('Global UI Test teardown begins')

    cleanup_before_after_all(driver)

    logging.info('Global UI Test teardown has finished')


def setup_ui_test(request) -> webdriver:
    logging.info("UI Test Setup Begins")
    driver = setup_webdriver()
    request.session.driver = driver
    logging.info("UI Test Setup Finished")
    return driver


def cleanup_before_after_all(driver):
    if driver is not None:
        logging.info("driver shutdown")
        driver.quit()
        time.sleep(2)

    print("\n\n** View artifact(s) test.log or report.html for more thorough test logs and information **")
