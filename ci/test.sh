#!/bin/bash

set -e

pip3 install -r requirements.txt

pytest test/* -s -v --color=yes --reruns 3 --reruns-delay 2