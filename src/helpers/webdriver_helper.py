import logging
import os

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from src.helpers.time_formatter import TimeFormatter


def setup_webdriver() -> webdriver:
    driver = None
    browser = os.getenv('BROWSER')
    logging.info(f"initiating {browser} driver")
    if browser.upper() == 'CHROME':
        driver = setup_chrome()
    elif browser.upper() == 'FIREFOX':
        driver = setup_firefox()
    else:
        logging.debug("no valid Browser was provided to set up. Check the gitlab ci variables.")

    return driver


def setup_chrome() -> webdriver:
    options = webdriver.ChromeOptions()
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--headless")  # this can be commented out for local development/testing
    driver = webdriver.Chrome(
        executable_path=ChromeDriverManager().install(),
        options=options
    )
    return driver


def setup_firefox() -> webdriver:
    options = webdriver.FirefoxOptions()
    options.headless = "True"
    driver = webdriver.Firefox(
        executable_path=GeckoDriverManager().install(),
        options=options,
        service_log_path=os.devnull
    )
    return driver


def take_screenshot(driver: webdriver, test_name: str):
    screenshot_dir = f"{os.getcwd()}/screenshots/"
    if not os.path.isdir(screenshot_dir):
        os.mkdir('screenshots/')
    screenshots_path = f"{screenshot_dir}{TimeFormatter.millis()}-{test_name}.png"
    driver.save_screenshot(screenshots_path)
    logging.info(f"{test_name} failed taking screenshot {screenshots_path}")
