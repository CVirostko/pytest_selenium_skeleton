from datetime import datetime


class TimeFormatter:
    @staticmethod
    def suffix(day):
        return 'th' if 11 <= day <= 13 else {1: 'st', 2: 'nd', 3: 'rd'}.get(day % 10, 'th')

    @staticmethod
    def custom_strftime(dt_format, time):
        return time.strftime(dt_format).replace('{S}', str(time.day) + TimeFormatter.suffix(time.day))

    @staticmethod
    def millis() -> int:
        return int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000)
