import logging

def setup_basic_console_logger(log_level: str):
    logging.basicConfig(level=log_level,
                        format='[%asctime)s - %(levelname)s]: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
