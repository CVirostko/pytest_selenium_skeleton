import logging
import time

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions


class BasePageObject(object):

    def __init__(self, request):
        self._driver = request.driver

    def click(self, by=None, value=None, web_element=None, wait_time=2):
        """
        click method wrapped in a WebDriverWait to wait for the wait_time to exist

        Args:
            by: by locator for the element you wish to click
            value: value locator for the element you wish to click
            web_element: if not passing in a by/value tuple pass in the WebElement
            wait_time: time to wait for the element to be available for automation
        """
        logging.info("performing click on ({0}, {1})".format(by if web_element is None else "web_element",
                                                             value if web_element is None else web_element.tag_name))
        element = WebDriverWait(self._driver, wait_time).until(
            expected_conditions.element_to_be_clickable((by, value)) if web_element is None
            else expected_conditions.visibility_of(web_element)
        )
        element.click()

    def submit(self, by=None, value=None, web_element=None, wait_time=2):
        """
        submit method wrapped in a WebDriverWait to wait for the wait_time to exist

        Args:
            by: by locator for the element you wish to submit
            value: value locator for the element you wish to submit
            web_element: if not passing in a by/value tuple pass in the WebElement
            wait_time: time to wait for the element to be available for automation
        """
        logging.info("performing submit on ({0}, {1})".format(by if web_element is None else "web_element",
                                                              value if web_element is None else web_element.tag_name))
        element = WebDriverWait(self._driver, wait_time).until(
            expected_conditions.presence_of_element_located((by, value)) if web_element is None
            else expected_conditions.visibility_of(web_element)
        )
        element.submit()

    def clear(self, by, value, wait_time=2):
        """
        clear method wrapped in a WebDriverWait to wait for the wait_time to exist

        Args:
            by: by locator for the element you wish to clear
            value: value locator for the element you wish to clear
            wait_time: time to wait for the element to be available for automation
        """
        logging.info("performing clear on ({0}, {1})".format(by, value))
        element = WebDriverWait(self._driver, wait_time).until(
            expected_conditions.presence_of_element_located((by, value))
        )
        element.clear()

    def type(self, by, value, text: str, wait_time=2):
        """
        type the text into the element provided

        Args:
            by: by locator for the element you wish to type into
            value: value locator for the element you wish to type into
            text: text you will be typing into the element
            wait_time: time to wait for the element to be available for automation
        """
        logging.info("setting text of ({0}, {1}) to {2}".format(by, value, text))
        element = WebDriverWait(self._driver, wait_time).until(
            expected_conditions.presence_of_element_located((by, value))
        )
        element.send_keys(text)

    def get_text(self, by, value, wait_time=2) -> str:
        """
        get the text of the element

        Args:
            by: by locator for the element you wish to get text for
            value: value locator for the element you wish to get text for
            wait_time: time to wait for the element to be available for automation

        Returns:
            elements text
        """
        element = WebDriverWait(self._driver, wait_time).until(
            expected_conditions.presence_of_element_located((by, value))
        )
        return element.text

    def get_attribute(self, by, value, attribute: str, wait_time=2) -> str:
        """
        get the value of the attribute passed in for the element provided

        Args:
            by: by locator for the element you wish to get attribute for
            value: value locator for the element you wish to get attribute for
            attribute: attribute that you want to get from the element
            wait_time: time to wait for the element to be available for automation

        Returns:
            value from the element specified
        """
        element = WebDriverWait(self._driver, wait_time).until(
            expected_conditions.presence_of_element_located((by, value))
        )
        return element.get_attribute(attribute)

    def get_element(self, by, value, wait_time=2):
        """
        get the element

        Args:
            by: by locator for the element you wish to locate
            value: value locator for the element you wish to locate
            wait_time: time to wait for the element to be available for automation

        Returns:
            web element
        """
        element = WebDriverWait(self._driver, wait_time).until(
            expected_conditions.presence_of_element_located((by, value))
        )
        return element

    def exists(self, by, value, wait_time=2):
        """
        validate whether or not an element exists

        Args:
            by: by locator for the element you wish to locate
            value: value locator for the element you wish to locate
            wait_time: time to wait for the element to be available for automation

        Returns:
            boolean related to existence
        """
        try:
            WebDriverWait(self._driver, wait_time).until(
                expected_conditions.presence_of_element_located((by, value))
            )
            logging.info("element ({0}, {1}) found".format(by, value))
            return True
        except (NoSuchElementException, TimeoutException):
            logging.info("element ({0}, {1}) not found".format(by, value))
            return False

    def scroll_into_view(self, element):
        """
        execute javascript to scroll an element into view if required

        Args:
            element: element you wish to scroll into view
        """
        self._driver.execute_script("arguments[0].scrollIntoView();", element)


def truncate(value_to_truncate: str, max_length: int = 18) -> str:
    """
    truncate the value passed in to the max_length provided and add an ellipsis on the end

    Args:
        value_to_truncate: value you want to truncate to length provided
        max_length: max length you want the value passed end, following characters will be removed and ellipsis

    Returns:
        the truncated string if greater than then max_length otherwise just return the string
    """
    if len(value_to_truncate) > max_length:
        return value_to_truncate[:max_length] + '…'
    else:
        return value_to_truncate


def hard_sleep(seconds: float):
    """
    hard sleep [only when absolutely necessary]

    Args:
        seconds: number of seconds to hard sleep for
    """
    time.sleep(seconds)
