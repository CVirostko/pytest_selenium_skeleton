from src.page_objects.base_pageobject import BasePageObject
from src.page_objects.google.google_locators import GooglePageLocators


class GooglePage(BasePageObject):

    def navigate(self):
        """
        navigate to this page
        """
        self._driver.get('https://www.google.com')

    def logo_exists(self) -> bool:
        """
        check whether the page logo exists

        Returns:
            boolean value based on object existence
        """
        return self.exists(*GooglePageLocators.LOGO)

    def get_page_title(self) -> str:
        """
        get the title of the current page from the webdriver

        Returns:
            title of the current page
        """
        return self._driver.title

    def get_search_text(self) -> str:
        """
        get the text of the search input

        Returns:
            text displayed in the search input field
        """
        return self.get_text(*GooglePageLocators.SEARCH_INPUT)

    def search_results_exist(self) -> bool:
        """
        check whether the search results exist

        Returns:
            boolean value based on object existence
        """
        return self.exists(*GooglePageLocators.SEARCH_RESULTS)

    def set_search_text(self, search_text: str):
        """
        set the text of the search input to the value provided

        Args:
            search_text: text that you want to search for
        """
        self.type(*GooglePageLocators.SEARCH_INPUT, search_text)

    def search_btn_click(self):
        """
        click the search button for the search input
        """
        self.submit(*GooglePageLocators.SEARCH_BTN)
