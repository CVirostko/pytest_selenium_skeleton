from selenium.webdriver.common.by import By


class GooglePageLocators:

    LOGO = (By.XPATH, '//img[@alt="Google"]')
    SEARCH_INPUT = (By.XPATH, '//input[@name="q"]')
    SEARCH_BTN = (By.XPATH, '//input[@name="btnK"]')
    SEARCH_RESULTS = (By.XPATH, '//div[@id="search"]')
